<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<section class="main-container col1-layout home-content-container">
    <div class="container">
      <div class="row">
        <div class="std">
          <div class="best-pro col-lg-12">
            <div class="slider-items-products">
              <div class="new_title center">
                <h2>Club Clothes</h2>
              </div>
              <div id="best-seller-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4"> 
                  
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/Arsenal2.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"> <span> Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="product_detail.html" title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price" id="product-price-1"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/Bacelona1.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:0%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box">
                                  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price" id="old-price-2"> $567.00 </span> </p>
                                  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price" id="product-price-2"> $456.00 </span> </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/MCclothes.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:20%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box">
                                  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $167.00 </span> </p>
                                  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $99.00 </span> </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="grid.html"  class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                      <strong></strong> </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/MUClothesRed.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:0%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$225.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/LPClothesBlack.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/product6.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
          </div>
          <div class="featured-pro col-lg-12">
            <div class="slider-items-products">
              <div class="new_title center">
                <h2>National Clothes</h2>
              </div>
              <div id="featured-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4"> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/VNClothes.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html" class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/SpainClothes.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html"  class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:0%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box">
                                  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $567.00 </span> </p>
                                  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $456.00 </span> </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/FranceClothes.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html"  class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:20%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box">
                                  <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> $167.00 </span> </p>
                                  <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> $99.00 </span> </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                      <strong></strong> </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/Argentina.jpg" alt="Sample Product"></a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html"  class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:0%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$225.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/product_Brazil1.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html"  class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">
                          <div class="item-img-info"> <a href="product_detail.html"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/BiClothes.jpg" alt="Sample Product"> </a>
                            <div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="quick_view.html"  class="button detail-bnt"><span>Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="grid.html"  title="Sample Product"> Sample Product </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item --> 
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End main container --> 
  
  <!-- Latest Blog -->
  <section class="latest-blog wow bounceInDown animated">
    <div class="container">
      <div class="row">
        <div class="new_title center">
          <h2><span>Shoes and Ball</span></h2>
        </div>
        <div class="col-xs-12 col-sm-3">
          <div class="blog_inner">
            <div class="blog-img blog-l"> <img src="<%=request.getContextPath() %>/resources/images/ronaldoShoes.jpg" alt="Blog image">
              <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3">
          <div class="blog_inner">
            <div class="blog-img blog-l"> <img src="<%=request.getContextPath() %>/resources/images/ball2.jpg" alt="Blog image">
              <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3">
          <div class="blog_inner">
            <div class="blog-img blog-l"> <img src="<%=request.getContextPath() %>/resources/images/messiShoes.jpg" alt="Blog image">
              <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3">
          <div class="blog_inner">
            <div class="blog-img blog-l"> <img src="<%=request.getContextPath() %>/resources/images/ball1.jpg" alt="Blog image">
              <div class="mask"> <a class="info" href="blog_detail.html">Read More</a> </div>
            </div>
              
          </div>
        </div>
      </div>
    </div>
  </section>
  
