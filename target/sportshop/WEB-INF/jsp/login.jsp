
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>

<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Google Fonts -->


<link type ="text/css" rel="stylesheet" href ="<%=request.getContextPath() %>/resources/css/login.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/slider.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.theme.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/style.css" type="text/css">

</head>

<body class="cms-index-index">
	<div class="page">
		<!-- Header -->
		<header class="header-container">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-xs-6">

							<div class="welcome-msg hidden-xs">
								<h2>Welcome to SD Shop</h2>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="header container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3">
						<!-- Header Logo -->
						<div class="logo">
							<a title="Magento Commerce" href=""><img
								alt="Magento Commerce"
								src="<%=request.getContextPath()%>/resources/images/image_SDshop.png"></a>
						</div>
						<!-- End Header Logo -->
					</div>
					<div class="col-lg-9 col-xs-12">
						<div class="top-cart-contain">
							<div class="mini-cart">
								<div data-toggle="dropdown" data-hover="dropdown"
									class="basket dropdown-toggle">
									<a href="#"><span>Shopping Cart</span></a>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- end header -->
		<!-- Navbar -->
		<%@include file="menu.jsp"%>
		<div class="container">
			<div class="card card-container">
				<img id="profile-img" class="profile-img-card"
					src="<%=request.getContextPath()%>/resources/images/login.png" />
				<p id="profile-name" class="profile-name-card"></p>
				<form:form class="form-signin" action="loginPost"
					modelAttribute="staffInformation" method="POST"
					enctype="multipart/form-data">
					<tr>
						<td align="left" width="40%"><input type="text"
							name="userName" class="form-control" placeholder="User Name"
							required autofocus /></td>
						<td align="left"><form:errors path="userName"
								cssClass="error" /></td>
					</tr>
					<tr>
						<td align="left" width="40%"><input type="password"
							name="password" class="form-control" placeholder="Password"
							required />
						<td align="left"><form:errors path="password"
								cssClass="error" /></td>
					</tr>
					<tr>
						<td align="center"><button
								class="btn btn-lg btn-primary btn-block btn-signin"
								type="submit">LOGIN</button></td>
					</tr>
				</form:form>
				<tr>

					<td>
						<button class="btn btn-lg btn-primary btn-block btn-signin"
							value="Back" onclick="javascript:history.go(-1)">BACK</button>
					</td>
				</tr>
				<%@include file="message.jsp"%><!-- /form -->
			</div>
			<!-- /card-container -->
		</div>



		<!-- Footer -->
		<%@include file="footer.jsp"%>
		<!-- End Footer -->

	</div>



	<script type="text/javascript"

		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(function() {
			jQuery(".slideshow").cycle({
				fx : 'scrollHorz',
				easing : 'easeInOutCubic',
				timeout : 10000,
				speedOut : 800,
				speedIn : 800,
				sync : 1,
				pause : 1,
				fit : 0,
				pager : '#home-slides-pager',
				prev : '#home-slides-prev',
				next : '#home-slides-next'
			});
		});
		//]]>
	</script>
</body>