<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>edit Account</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/slider.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.theme.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/style.css"
	type="text/css">
<!-- Google Fonts -->
<style>
table {
	border-collapse: collapse;
	width: 50%;
	height: 400px;
	font-size: 15px;
}

th, td {
	text-align: center;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #eacece
}
</style>
</head>
<body>
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
  <center><h3><b>Edit Account</b></h3></center>
 <div class="container">
			<div class="card card-container"> 
	<!-- <form class="form-signin" action="userController" method="GET"> -->
	
	<center>
		<form action="editInformationOfStaff" modelAttribute = "staffInfor"
		 method="POST" enctype="multipart/form-data">
			<%@include file="message.jsp"%>
			<table border="3" cellpadding="5" cellspacing="1">
				<tr>
					<td><b>Name</b></td>
					<td><b>${edit.name}</b></td>
				</tr>
				<tr>
					<td><b></>Address</b></td>
					<td><b><input type="text" name="address" value="${edit.address}"  /></b></td>
					<form:errors path="address"
								cssClass="error" />
				</tr>
				<tr>
					<td><b>Phone</b></td>
					<td><b><input type="text" name="phone" value="${edit.phone}" /></b></td>
					<form:errors path="phone"
								cssClass="error" />
				</tr>
				<tr>
					<td><b>Gender</b></td>
					<td><b><c:choose>
							<c:when test="${edit.gender eq true}"> Male  <br />

							</c:when>
							<c:otherwise> Female <br />
							</c:otherwise>
						</c:choose></b></td>
				</tr>
				<tr>
					<td><b>Identity Card</b></td>
					<td><b>${edit.identityCard}</b></td>
				</tr>
				<tr>
					<td><b>UserName</b></td>
					<td><b><input type="text" name="userName"
						value="${edit.userName}" /></b></td>
						<form:errors path="userName"
								cssClass="error" />
				</tr>
				<tr>
					<td><b>Password</b></td>
					<td><b><input type="password" name="password"
						value="${edit.descriptionPassword}" /></b></td>
					<form:errors path="descriptionPassword"
								cssClass="error" />
				</tr>
				<tr>
					<td><b>Role</b></td>
					<td><b><c:choose>
							<c:when test="${edit.role.roleId eq '2'}"> Boss<br />
							</c:when>
							<c:otherwise>Staff<br />
							</c:otherwise>
						</c:choose></b></td>
				</tr>
			</table>
			</br>
			<button type="submit" style="background-color: #3a96f2" class="button btn-cart"><b>EDIT</b></button>
			<input type="hidden" name="id" value="${edit.staffId}"></input>
		</form>
			<button type="submit" style="background-color: #3a96f2 " class="button btn-cart" value="Back" onclick="javascript:history.go(-1)"><b>BACK</b></button>
	</center>
</div>
</div>
<%@include file="footer.jsp"%>
</div>
</body>
</html>