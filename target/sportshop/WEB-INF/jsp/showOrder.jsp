<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>User Account</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/slider.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.theme.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/style.css"
	type="text/css">
<style>
table {
	border-collapse: collapse;
	width: 105%;
	font-size: 15px;
}

th, td {
	text-align: center;
	padding: 5px;
}
</style>
</head>
<body class="cms-index-index">
	<div class="page">
		<%@include file="header1.jsp"%>
		<%@include file="menu.jsp"%>
		<center>
			<h1>ORDER</h1>
		</center>
		<div class="container">
			<div class="card card-container">
				<center><%@include file="message.jsp"%></center>
				</br>
				<table border="5">
					<tr>
						<th><b>Order ID</b></th>
						<th><b>Date Order</b></th>
						<th><b>Customer Name</b></th>
						<th><b>Address</b></th>
						<th><b>Phone</b></th>
						<th><b>Staff ID</b></th>
						<th><b>Product ID</b></th>
						<th><b>Quantity</b></th>
						<th><b>Total</b></th>
						<th><b>Status</b></th>
						<th><b></b></th>
					</tr>
					<c:forEach items="${order}" var="order">
						<tr>
							<td><b><c:out value="${order.orderId}" /></b></td>
							<td><b>${order.orderDate}</b></td>
							<td><b>${order.customerName}</b></td>
							<td><b>${order.address}</b></td>
							<td><b>${order.phone}</b></td>
							
							<td><b><c:choose>
										<c:when test="${order.staffInformation.staffId == 1}">No One<br />
										</c:when>
										<c:otherwise>${order.staffInformation.staffId}<br />
										</c:otherwise>
									</c:choose></b></td>
							<td>
								<table>
									<tr>
										<c:forEach items="${order.products}" var="productOrder">
											<tr>
												<td><b>${productOrder.product.productId}</b></td>
											</tr>
										</c:forEach>
									</tr>
								</table>
							</td>
							<td>
								<table>
									<tr>
										<c:forEach items="${order.products}" var="productOrder">
											<tr>
												<td><b>${productOrder.product.quantity}</b></td>
											</tr>
										</c:forEach>
									</tr>
								</table>
							</td>
							<td><b>${order.total}00VNĐ</b></td>
							<td><b><c:choose>
										<c:when test="${order.status == 'true'}">HOÀN THÀNH<br />
										</c:when>
										<c:otherwise>CHƯA HOÀN THÀNH<br />
										</c:otherwise>
									</c:choose></b></td>
							<td><a
								href="<%=request.getContextPath()%>/product/sellProduct?staffId=${staffId}
									&orderId=${order.orderId}&status=${order.status}">
									<button style="background-color:"class="button btn-cart" type="submit">
										<b>HOÀN THÀNH</b>
									</button>
							</a> <a
								href="<%=request.getContextPath()%>/product/deleteOrder?orderId=${order.orderId}">
									<button style="background-color:" class="button btn-cart" type="submit">
										<b>HỦY ĐƠN HÀNG</b>
									</button>
							</a></td>
						</tr>
					</c:forEach>
				</table>
				<center>
					<tr>
						<td><h3>
								<B><input style="background-color:" class="button btn-cart"
									type="button" value="Back" onclick="javascript:history.go(-1)"></B>
							</h3></td>
					</tr>
				</center>
			</div>
		</div>
		<%@include file="footer.jsp"%>
	</div>
</body>
</html>