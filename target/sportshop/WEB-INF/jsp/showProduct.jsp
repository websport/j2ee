<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.pnv.model.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8 ">
<title>Show all Product</title>
<!-- Favicons Icon -->
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/style.css" type="text/css">

<!-- Google Fonts -->

<style>
table {
    border-collapse: collapse;
    width: 70%;
    font-size: 15px;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body class="cms-index-index">
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
	<center>
		<h1><b>Show Product</b></h1>
		 <p style="color: red;">${MS}</p>
	</center>
   <a href="<%=request.getContextPath()%>/product/add" class="button">Add New</a>
   
	<p style="color: red;">${errorString}</p>
	<center>
		<table border="5" cellpadding="5" cellspacing="1">
		<tr>	 <th>productId</th>
			<th>productName</th>
			<th>quantity</th>
			<th>dateUpdate</th>
			<th>sizeId</th>
			<th>price</th>
			<th>description</th>
			<th>image</th>
            <th>Action</th>
			</tr>
			  <c:forEach items="${product_list}" var="product">  
        <tr>  
            <td><c:out value="${product.productId}"/></td>  
            <td><c:out value="${product.productName}"/></td>  
            <td><c:out value="${product.quantity}"/></td>  
            <td><c:out value="${product.dateUpdate}"/></td>
            <td><c:out value="${product.size.typeOfSize}"/></td>
            <td><c:out value="${product.price}"/></td>
            <td><center><c:out value="${product.description}"/> </center></td>
            <td><img height="50px" width="50px" src="<c:url value="/resources/images/${product.image}" />"></td> 
            <td align="center"><a href="<%=request.getContextPath()%>/product/edit?id=${product.productId}">Edit</a> | <a href="<%=request.getContextPath()%>/product/delete?id=${product.productId}">Delete</a></td>
            
        </tr>  
    </c:forEach> 
	</table>
		<center>
			<tr>
				<td><b><input style="background-color: " type="button" 
					value="BACK" onclick="javascript:history.go(-1)"></b></td>
			</tr>
		</center>
	</center>
	<%@include file="footer.jsp"%>
</body>
</html>