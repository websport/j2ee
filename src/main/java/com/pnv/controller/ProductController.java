package com.pnv.controller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.pnv.business.ProductBusiness;
import com.pnv.model.Order;
import com.pnv.model.Product;

@Service
@Transactional
@Controller
@RequestMapping(value = "/product")
public class ProductController {

	@Autowired
	private ProductBusiness show;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String viewDepartmentPage(ModelMap map) {
		List<Product> product_list = show.findAll();
		map.put("product_list", product_list);
		return "showProduct";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String viewEditTitlePage(@PathVariable("id") Integer id, ModelMap map) {
		Product productForm = show.findByProductId(id);
		map.addAttribute("productForm", productForm);
		map.put("MS", "Edit sucessfully");
		return "insertProduct";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String viewAddNewPage(ModelMap map) {
		Product productForm = new Product();
		map.addAttribute("productForm", productForm);
		return "insertProduct";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String doAddNew(@Valid @ModelAttribute("productForm") Product productForm, BindingResult result,
			ModelMap map, @RequestParam("file") MultipartFile file, HttpServletRequest request) throws IOException {

		if (result.hasErrors()) {
			map.addAttribute("productForm", productForm);
			return "insertProduct";
		}

		if (!file.isEmpty()) {
			HttpSession session = request.getSession();
			ServletContext sc = session.getServletContext();
			String imagePath = sc.getRealPath("/") + "/resources/images/";
			File theDir = new File(imagePath);
			if (!theDir.exists()) {
				System.out.println("creating directory: " + imagePath);
				boolean isCreated = false;

				try {
					theDir.mkdir();
					isCreated = true;
				} catch (SecurityException se) {
					// handle it
				}
				if (isCreated) {
					System.out.println("DIR created");
				}
			}
			System.out.println(imagePath);
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (file.getSize() > 0) {
				inputStream = file.getInputStream();
				File newFile = new File(imagePath + file.getOriginalFilename());
				if (!newFile.exists()) {
					newFile.createNewFile();
				}
				outputStream = new FileOutputStream(imagePath + file.getOriginalFilename());
				System.out.println(file.getOriginalFilename());
				int readBytes = 0;
				byte[] buffer = new byte[8192];
				while ((readBytes = inputStream.read(buffer, 0, 8192)) != -1) {
					outputStream.write(buffer, 0, readBytes);
				}
				outputStream.close();
				inputStream.close();
				productForm.setImage(file.getOriginalFilename());
			}
		}
		show.saveOrUpdate(productForm);
		List<Product> product_list = show.findAll();
		map.put("product_list", product_list);
		map.addAttribute("MS", "Add product sucessfully");
		return "showProduct";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String doDeleteTitle(@PathVariable("id") Integer id, ModelMap map) {
		show.delete(show.findByProductId(id));
		map.addAttribute("MS", "Delete sucessfuly");
		List<Product> product_list = show.findAll();
		map.put("product_list", product_list);
		return "showProduct";
	}

	@RequestMapping(value = "/showInformation/{id}", method = RequestMethod.GET)
	public String viewInfomationProduct(@PathVariable("id") Integer id, ModelMap map) {
		Product productForm = show.findByProductId(id);
		map.addAttribute("showPr", productForm);
		return "informationProduct";
	}

	@RequestMapping(value = "/showProductMenu/{name}", method = RequestMethod.GET)
	public String viewInfomationProduct(@PathVariable("name") String name, ModelMap map) {

		List<Product> productForm = show.findAllMenu(name);
		map.addAttribute("ListClub", productForm);
		return "productMenu";
	}

	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String order(ModelMap map, HttpServletRequest request) {
		List<Order> list = show.showOrder();
		HttpSession session = request.getSession();
		int staffId = (Integer) session.getAttribute("staffId");
		map.addAttribute("order", list);
		map.addAttribute("staffId", staffId);
		return "showOrder";
	}

	@RequestMapping(value = "/sellProduct/{staffId}/{orderId}/{status}", method = RequestMethod.GET)
	public String sellProduct(ModelMap map, @PathVariable("staffId") Integer staffId, @PathVariable("orderId") Integer orderId,
			@PathVariable("status") boolean status, HttpServletRequest request) {
		if (!status) {
			int result = show.sellProduct(staffId, orderId, true);
			if (result != 0) {
				List<Order> list = show.showOrder();
				map.addAttribute("order", list);
				map.addAttribute("MS", "-------ORDER THÀNH CÔNG----------");
				return "showOrder";
			} else {
				List<Order> list = show.showOrder();
				map.addAttribute("order", list);
				map.addAttribute("MS", "-------ORDER KHÔNG THÀNH CÔNG-------");
				return "showOrder";
			}
		}
		List<Order> list = show.showOrder();
		map.addAttribute("order", list);
		map.addAttribute("MS", "--------ĐƠN HÀNG ĐÃ ĐƯỢC ORDER---------");
		return "showOrder";
	}

	@RequestMapping(value = "/deleteOrder/{orderId}", method = RequestMethod.GET)
	public String deleteOrder(@PathVariable("staffId") Integer orderId, ModelMap map) {

		int result = show.deleteOrder(orderId);
		if (result != 0) {
			List<Order> list = show.showOrder();
			map.addAttribute("order", list);
			map.addAttribute("MS", "------------HỦY THÀNH CÔNG-------------");
			return "showOrder";
		}
		List<Order> list = show.showOrder();
		map.addAttribute("order", list);
		map.addAttribute("MS", "------------HỦY KHÔNG THÀNH CÔNG-------------");
		return "showOrder";

	}
	@RequestMapping(value = "/seaching", method = RequestMethod.POST)
	public String doSearching(@RequestParam(value = "searchproduct", required = true) String text, ModelMap map) {
		   List<Product> product_list = show.searchAll(text);
		   map.put("data", product_list);
		   return "searchProduct";
	}

}
