package com.pnv.business;

import java.util.List;

import com.pnv.model.Order;
import com.pnv.model.Product;

public interface ProductBusiness {
	
	 public void saveOrUpdate(Product product);
	 
	 public void delete(Product product);
	 
	 public List<Product> findAll();
	 
	 public Product findByProductId(int id);
	 
	 public Product findProductCode(String ProductCode);
	 
	 public List<Product> showProductClub();
	 
	 public List<Product> showProductSB();
	 
	 public List<Product> showProductNational();
	 
	 public List<Product> findAllMenu(String nameProduct);
	 
	 List<Order> showOrder();
	 
	 int sellProduct(int staffId, int orderId, boolean status);
	 
	 int deleteOrder(int orderId);
	 
	 public List<Product> searchAll(String value);
}
