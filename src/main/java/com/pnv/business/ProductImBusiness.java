package com.pnv.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.ProductDao;
import com.pnv.dao.ProductImDao;
import com.pnv.model.Order;
import com.pnv.model.Product;

@Service
@Transactional
public class ProductImBusiness implements ProductBusiness {
	
	@Autowired
	ProductDao productDao;
	
	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}
	
	public void saveOrUpdate(Product product) {
		productDao.saveOrUpdate(product);	
	}

	public void delete(Product product) {
		productDao.delete(product);
	}

	public List<Product> findAll() {
		return productDao.findAll();
	}

	public Product findByProductId(int id) {
		return productDao.findByProductId(id);
	}

	public Product findProductCode(String ProductCode) {
		return productDao.findProductCode(ProductCode);
	}

	public List<Product> showProductClub() {
		return productDao.showProductClub();
	}

	public List<Product> showProductSB() {
		return productDao.showProductSB();
	}

	public List<Product> showProductNational() {
		return productDao.showProductNational();
	}

	public List<Product> findAllMenu(String nameProduct) {
		return productDao.findAllMenu(nameProduct);
	}

	public List<Order> showOrder() {
		List<Order> list = productDao.showOrder();
		for (int i = 0; i < list.size(); i++) {
			list.get(i).setProducts(productDao.showProductOrder(list.get(i).getOrderId()));
		}
		return list;
	}

	public int sellProduct(int staffId, int orderId, boolean status) {
		
		int result = productDao.sellProduct(staffId, orderId, status);
		return result;
	}

	public int deleteOrder(int orderId) {
		int result = productDao.deleteOrder(orderId);
		return result;
	}

	public List<Product> searchAll(String value) {
		return productDao.searching(value);
	}

}
