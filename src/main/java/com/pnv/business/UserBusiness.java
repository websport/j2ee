package com.pnv.business;

import java.util.List;

import com.pnv.model.StaffInformation;

public interface UserBusiness {

	// boolean createNewStaff(String name, String address, String phone, boolean
	// gender, String indentityCard,
	// String userName, String password, int role);
	int checkLogin(String userName, String password);

	boolean changePassword(String userName, String password, String newpassword);
	
	List<StaffInformation> showStaff();
	
	 boolean deleteStaffById(int id);
	
	 StaffInformation findUser(int staffId);
	 
	boolean changeInformationOfStaff(int id,String address, String phone,String userName, String password);
	boolean saveOrUpdate(StaffInformation staff);
    StaffInformation changeInforOfStaff(int id,String name,String address,String username,String passwork  );

	
	StaffInformation checkLogin1(String userName, String password);
	
}
