package com.pnv.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.UserDao;
import com.pnv.dao.UserImpDao;
import com.pnv.model.StaffInformation;
import com.pnv.utils.Utils;

@Service
@Transactional
public class UserImpBusiness implements UserBusiness {
	@Autowired
	UserDao user;

	// public boolean createNewStaff(String name, String address, String phone,
	// boolean gender, String identityCard,
	// String userName, String password, int role) {
	//
	// //StaffInformation staff = new
	// StaffInformation(name,address,phone,gender,identityCard,userName,password,role);
	//
	// UserImpDao newStaff = new UserImpDao();
	//
	// boolean result = newStaff.createNewStaff(staff);
	//
	// if(result){
	// return true;
	// }
	// return false;
	// }
	//
	//
	public int checkLogin(String userName, String password) {

		String pwmd5 = Utils.getMD5(password);

		int staffId = user.selectStaffId(userName, pwmd5);

		if (staffId != 0) {
			return staffId;
		}
		return 0;
	}

	public boolean changePassword(String userName, String password, String newPassword) {
		boolean result = user.updatePassword(userName, password, newPassword);
		if (result) {
			return true;
		}
		return false;
	}

	public List<StaffInformation> showStaff() {
		List<StaffInformation> staff = new ArrayList<StaffInformation>();
		staff = user.queryStaff();
		return staff;
	}

	public boolean deleteStaffById(int id) {
		boolean deleteAccount = user.deleteStaffById(id);
		if (deleteAccount) {
			return true;
		}
		return false;
	}

	public StaffInformation findUser(int staffId) {
		StaffInformation show = user.showStaff(staffId);
		return show;
	}

	public boolean changeInformationOfStaff(int id, String address, String phone, String userName, String password) {
		boolean result = user.updateInformationOfStaff(id, address, phone, userName, password);
		if (result) {
			return true;
		}
		return false;
	}

	public boolean saveOrUpdate(StaffInformation staff) {
		user.saveOrUpdate(staff);
		boolean result = true;
		if (result) {
			return true;
		}
		return false;
	}

	public StaffInformation checkLogin1(String userName, String password) {
		String pwmd5 = Utils.getMD5(password);

		StaffInformation staff = user.selectStaff(userName, pwmd5);

		if (staff != null) {
			return staff;
		}
		return null;
	}

	public StaffInformation changeInforOfStaff(int id, String name, String address, String userName, String password) {

		return user.updateInfoOfStaff(id, name, address, userName, password);

	}

}
