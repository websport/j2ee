package com.pnv.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidation {

	public static String phoneNumberInputValidation(String phoneNumber) {
		String errorMessage = "";

		if (phoneNumber.equals("")) {
			errorMessage = "Phone number can not be blank!";
		} else {
			try {
				double d = Double.parseDouble(phoneNumber);
			} catch (NumberFormatException nfe) {
				errorMessage = "Phone number must be between 0-9!";
			}
		}
		return errorMessage;
	}
	public static String identityCardInputValidation(String phoneNumber) {
		String errorMessage = "";

		if (phoneNumber.equals("")) {
			errorMessage = "Identity card number can not be blank!";
		} else {
			try {
				double d = Double.parseDouble(phoneNumber);
			} catch (NumberFormatException nfe) {
				errorMessage = "Identity card number must be between 0-9!";
			}
		}
		return errorMessage;
	}

	public static String emailInputValidation(String email) {
		String errorMessage = "";

		Pattern pattern = Pattern.compile(new String(
				"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"));
		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches()) {
			errorMessage = "Email is invalid!";
		}
		return errorMessage;
	}

	public static String fullNameValidation(String input) {
		String errorMessage = "";

		
			try {
				double d = Double.parseDouble(input);
				errorMessage = "Full name can not number!";
			} catch (NumberFormatException nfe) {
				Pattern pattern = Pattern.compile(new String("^[^`~!@#$%^&*()-+|\\_\\=\\[\\]{};'\".></?]*$"));
				Matcher matcher = pattern.matcher(input);
				if (!matcher.matches()) {
					errorMessage = "Full name is invalid!";
				}

		}
		return errorMessage;
	}

	public static String addressValidation(String input) {
		String errorMessage = "";

		if (input.equals("")) {
			errorMessage = "Address can not be blank!";
		} else {
			try {
				double d = Double.parseDouble(input);
				errorMessage = "Address can not number";
			} catch (NumberFormatException nfe) {
				Pattern pattern = Pattern.compile(new String("^[^`~!@#$%^&*()-+|\\_\\=\\[\\]{};'\".></?]*$"));
				Matcher matcher = pattern.matcher(input);
				if (!matcher.matches()) {
					errorMessage = "Address is invalid!";
				}
			}

		}
		return errorMessage;
	}

	public static String passwordValidation(String input) {
		String errorMessage = "";
		if (input.equals("")) {
			errorMessage = "Password can not be blank!";
		}
		return errorMessage;
	}

	// Product ------------------------------------------------------//
	public static String productValidation(String price) {
		String errorMessage = "";

		if (price.equals("")) {
			errorMessage = "Price card can not be blank!";
		} else {
			try {
				float d = Float.parseFloat(price);
			} catch (NumberFormatException nfe) {
				errorMessage = "Price must is number!";
			}
		}
		return errorMessage;

	}

	public static String productQuantityValidation(String quantity) {
		String errorMessage = "";

		if (quantity.equals("")) {
			errorMessage = "Quantity card can not be blank!";
		} else {
			try {
				float d = Float.parseFloat(quantity);
			} catch (NumberFormatException nfe) {
				errorMessage = "Quantity must is number!";
			}

		}
		return errorMessage;
	}

	public static String productNameValidation(String input) {
		String errorMessage = "";
		if (input.equals("")) {
			errorMessage = "Product name can not be blank!";
		} else {
			Pattern pattern = Pattern.compile(new String("^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}"));
			Matcher matcher = pattern.matcher(input);
			if (!matcher.matches()) {
				errorMessage = "Product name is invalid!";
			}
		}
		return errorMessage;
	}
}
