package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "role")
public class Role {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	
	@Column(name = "role_id", unique = true, nullable = false)
	private int roleId;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "role")
	@JsonIgnore
	private List<StaffInformation> id;
	
	@Column (name = "name", unique = true)
	private String name;
	
	public Role(int roleId, String name) {
		super();
		this.roleId = roleId;
		this.name = name;
	}

	public Role() {
		super();
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
