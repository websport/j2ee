package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

//@Entity
//@Table(name = "product")

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")

public class Product implements java.io.Serializable {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "product_id", unique = true, nullable = true)
	private int productId;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
	@JsonIgnore
	private List<ProductOrder> productOrder;

	@NotEmpty
	@Column(name = "product_name", nullable = false, length = 45)
	private String productName;

	@NotNull
	@Column(name = "quantity", nullable = false, length = 45)
	private int quantity;

	@NotEmpty
	@Column(name = "date_update", nullable = false, length = 45)

	private String dateUpdate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "size_id", referencedColumnName = "size_id", nullable = false)
	@JsonIgnore
	private Size size;

//	@NotNull
//	@Column(name = "size_id", insertable = false, updatable = false)
//	private int size_id;

//	public int getSize_id() {
//		return size_id;
//	}

//	public void setSize_id(int size_id) {
//		this.size_id = size_id;
//	}
	@NotNull
	@Column(name = "price", nullable = false, length = 45)
	private float price;

	@NotEmpty
	@Column(name = "description", nullable = false, length = 455555)
	private String description;

	@Column(name = "image", nullable = false, length = 45)
	private String image;

	@Column(name = "status", nullable = false, length = 11)
	private int status;

	public Product(int productId, Integer quantity) {
		super();
		this.productId = productId;
		this.quantity = quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product(String productName, Integer quantity, String dateUpdate, Size size, float price, String description,
			String image, int status) {
		super();
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.size = size;
		this.price = price;
		this.description = description;
		this.image = image;
		this.status = status;
	}
	
	public Product(String productName, Integer quantity, String dateUpdate, Size size, float price, String description,
			int status) {
		super();
		this.productName = productName;
		this.quantity = quantity;
		this.dateUpdate = dateUpdate;
		this.size = size;
		this.price = price;
		this.description = description;
		this.status = status;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Product() {
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ProductOrder> getProductOrder() {
		return productOrder;
	}

	public void setProductOrder(List<ProductOrder> productOrder) {
		this.productOrder = productOrder;
	}

}
