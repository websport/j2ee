package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_customer", catalog = "database")
public class Order {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "order_id", unique = true, nullable = false)
	private int orderId;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
	@JsonIgnore
	private List<ProductOrder> productOrder;

	@Column(name = "date_order")
	private Date orderDate;

	@Column(name = "status")
	private boolean status = false;

	@Column(name = "total")
	private double total;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "staff_id", referencedColumnName = "staff_id", nullable = false)
	@JsonIgnore
	private StaffInformation staffInformation;

	public StaffInformation getStaffInformation() {
		return staffInformation;
	}

	public void setStaffInformation(StaffInformation staffInformation) {
		this.staffInformation = staffInformation;
	}

	@Column(name = "customer_name", length = 45)
	private String customerName;

	@Column(name = "address", length = 100)
	private String address;

	@Column(name = "phone", length = 11)
	private String phone;

	@Transient
	private List<ProductOrder> products = new ArrayList<ProductOrder>();
//	
//	
//	public Order(int orderId, Date orderDate, boolean status, double total, StaffInformation staffInformation,
//			String customerName, String address, String phone) {
//		super();
//		this.orderId = orderId;
//		this.orderDate = orderDate;
//		this.status = status;
//		this.total = total;
//		this.staffInformation = staffInformation;
//		this.customerName = customerName;
//		this.address = address;
//		this.phone = phone;
//	}

//	public Order(int orderId, Date orderDate, boolean status, double total, StaffInformation staffInformation,
//			String customerName, String address, String phone, List<ProductOrder> products) {
//		super();
//		this.orderId = orderId;
//		this.orderDate = orderDate;
//		this.status = status;
//		this.total = total;
//		this.staffInformation = staffInformation;
//		this.customerName = customerName;
//		this.address = address;
//		this.phone = phone;
//		this.products = products;
//	}

	public List<ProductOrder> getProducts() {
		return products;
	}

	public void setProducts(List<ProductOrder> products) {
		this.products = products;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<ProductOrder> getProductOrder() {
		return productOrder;
	}

	public void setProductOrder(List<ProductOrder> productOrder) {
		this.productOrder = productOrder;
	}

}
