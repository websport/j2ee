package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "size")
public class Size {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "size_id", unique = true, nullable = false)
	private int sizeId;

	@Column(name = "type_of_size", nullable = false, length = 45)
	private String typeOfSize;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "size")
	@JsonIgnore
	private List<Product> productId;

	public Size(int sizeId, String typeOfSize) {
		super();
		this.sizeId = sizeId;
		this.typeOfSize = typeOfSize;
	}

	public Size() {
		super();
	}

	public Size(int sizeId, String typeOfSize, List<Product> productId) {
		super();
		this.sizeId = sizeId;
		this.typeOfSize = typeOfSize;
		this.productId = productId;
	}

	public List<Product> getProduct() {
		return productId;
	}

	public void setProduct(List<Product> productId) {
		this.productId = productId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public String getTypeOfSize() {
		return typeOfSize;
	}

	public void setTypeOfSize(String typeOfSize) {
		this.typeOfSize = typeOfSize;
	}

}
