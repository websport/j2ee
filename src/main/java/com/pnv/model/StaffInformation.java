package com.pnv.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "staff_information", catalog = "database")
public class StaffInformation implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "staff_id", unique = true, nullable = false)
	private int staffId;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "staffInformation")
	@JsonIgnore
	private List<Order> orderCustomer;

	@Column(name = "name", length = 45)
	private String name;

	@Column(name = "address", length = 100)
	private String address;

	@Column(name = "phone", length = 11, unique = true)
	private String phone = "";

	@Column(name = "gender" , nullable = false)
	private boolean gender;

	@Column(name = "identity_card", unique = true, length = 9)
	private String identityCard;

	@Column(name = "username", length = 45, unique = true)
	private String userName = "";

	@Column(name = "password", length = 45)
	private String password = "";

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id", referencedColumnName = "role_id", nullable = false)
	private Role role;

	@Column(name = "description_password", unique = true)
	private String descriptionPassword;

	public StaffInformation() {
		super();
	}

	public List<Order> getOrderCustomer() {
		return orderCustomer;
	}

	public void setOrderCustomer(List<Order> orderCustomer) {
		this.orderCustomer = orderCustomer;
	}

	public StaffInformation(int staffId, String name, String address, String phone, boolean gender, String identityCard,
			String userName, String password, Role role) {
		super();
		this.staffId = staffId;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.identityCard = identityCard;
		this.userName = userName;
		this.password = password;
		this.role = role;
	}
    public StaffInformation(int staffId, String name, String address, String userName, String password) {
        //super();
        this.staffId = staffId;
        this.name = name;
        this.address = address;
        this.userName = userName;
        this.password = password;
    }

	public StaffInformation(String name, String address, String phone, boolean gender, String identityCard,
			String userName, String password, String description_password, Role role) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.identityCard = identityCard;
		this.userName = userName;
		this.password = password;
		this.descriptionPassword = description_password;
		this.role = role;
	}
	
	public String getDescriptionPassword() {
		return descriptionPassword;
	}

	public void setDescriptionPassword(String descriptionPassword) {
		this.descriptionPassword = descriptionPassword;
	}

	public StaffInformation(int staffId) {
		super();
		this.staffId = staffId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}
	

}
