package com.pnv.dao;

import java.util.List;

import com.pnv.model.StaffInformation;

public interface UserDao {
	
//	boolean createNewStaff(StaffInformation staffInformation);
	
	boolean deleteStaffById(int id);
	
	int selectStaffId(String userName, String password);
	
	boolean updatePassword(String userName, String password, String newPassword);
	
	List<StaffInformation> queryStaff();
	
	StaffInformation showStaff(int staffId);
	
	boolean updateInformationOfStaff(int id, String address, String phone, String userName, String password);

	StaffInformation updateInfoOfStaff(int id,String name, String address, String userName, String password);

	void saveOrUpdate(StaffInformation staffInformation);
	
	StaffInformation selectStaff(String userName, String password);
	
}
