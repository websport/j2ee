package com.pnv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.Entity;

import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.model.ProductOrder;
import com.pnv.model.StaffInformation;
import com.pnv.utils.DbHelper;

@Service
@Transactional
public class ProductImDao implements ProductDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void saveOrUpdate(Product product) {
		sessionFactory.getCurrentSession().saveOrUpdate(product);

	}

	public void delete(Product product) {

		sessionFactory.getCurrentSession().delete(product);

	}

	public List<Product> findAll() {

		List<Product> productList = sessionFactory.getCurrentSession().createQuery("from Product").list();
		return productList;
	}

	public Product findByProductId(int id) {

		Product product = null;

		product = (Product) sessionFactory.getCurrentSession().get(Product.class, id);

		return product;
	}

	public Product findProductCode(String ProductCode) {

		Product product = null;
		String strQuery = "from Product WHERE product_id = :Code ";
		Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
		query.setParameter("Code", ProductCode);
		product = (Product) query.uniqueResult();
		return product;
	}

	public List<Product> showProductClub() {
		List<Product> productArsenalList = sessionFactory.getCurrentSession()
				.createQuery("from Product where status='0' ORDER BY RAND()").list();
		return productArsenalList;
	}

	public List<Product> showProductSB() {
		List<Product> productSBList = sessionFactory.getCurrentSession()
				.createQuery("from Product where status='2' ORDER BY RAND()").list();
		return productSBList;
	}

	public List<Product> showProductNational() {
		List<Product> productNaList = sessionFactory.getCurrentSession()
				.createQuery("from Product where status='1' ORDER BY RAND()").list();
		return productNaList;
	}

	public List<Product> findAllMenu(String nameProduct) {
		List<Product> product = null;
		String strQuery = "from Product WHERE product_name = :Code ";
		Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
		query.setParameter("Code", nameProduct);
		product = query.list();
		return product;
	}

	public List<Order> showOrder() {
		String selectOrder = "from Order";
		List<Order> order = sessionFactory.getCurrentSession().createQuery(selectOrder).list();
		return order;
	}

	public List<ProductOrder> showProductOrder(int orderId) {
		String products = "from ProductOrder where order_id  = :Code";
		Query query = sessionFactory.getCurrentSession().createQuery(products);
		query.setParameter("Code", orderId);
		List<ProductOrder> product = query.list();
		return product;
	}

	public int sellProduct(int staffId, int orderId, boolean status) {

		String updateStaffId = "Update Order SET staffInformation.staffId = :staffId, status = :sta WHERE orderId = :order";
		Query query = sessionFactory.getCurrentSession().createQuery(updateStaffId);
		query.setParameter("staffId", staffId);
		query.setParameter("sta", status);
		query.setParameter("order", orderId);
		int check = query.executeUpdate();
		return check;
	}

	public int deleteOrder(int orderId) {
		String deletePro = "delete from ProductOrder WHERE order.orderId = :orderId";
		String deleteCusOrder = "DELETE FROM Order WHERE orderId = :orderId";

		sessionFactory.openSession().beginTransaction();
		Query query = sessionFactory.getCurrentSession().createQuery(deletePro);
		query.setParameter("orderId", orderId);
		query.executeUpdate();
		Query query1 = sessionFactory.getCurrentSession().createQuery(deleteCusOrder);
		query1.setParameter("orderId", orderId);
		int check = query1.executeUpdate();
		sessionFactory.openSession().beginTransaction().commit();
		return check;
	}

	
	public List<Product> searching(String values) {
//		List<Product> seachProduct = sessionFactory.getCurrentSession().createQuery(" from Product where productName LIKE :values").list();
//		
//		return seachProduct;
		
		String products = "from Product where productName LIKE :values";
		//String products = "from Product where productName MATCH(productName) AGAINST (':values' IN BOOLEAN MODE)";
		Query query = sessionFactory.getCurrentSession().createQuery(products);
		query.setParameter("values","%"+values+"%");
		List<Product> product = query.list();
		return product;
	}

}
