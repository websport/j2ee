package com.pnv.dao;

import java.util.ArrayList;
import java.util.List;

import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.model.ProductOrder;

public interface ProductDao {
	
	 public void saveOrUpdate(Product product);
	 
	 public void delete(Product product);
	 
	 public List<Product> findAll();
	 
	 public Product findByProductId(int id);
	 
	 public Product findProductCode(String ProductCode);
	 
	 public List<Product> showProductClub();
	 
	 public List<Product> showProductSB();
	 
	 public List<Product> showProductNational();
	 
	 public List<Product> findAllMenu(String nameProduct);
	 
	 List<Order> showOrder();
	 
	 List<ProductOrder> showProductOrder(int orderId);
	 
	 int sellProduct(int staffId, int orderId, boolean status);
	 
	 int deleteOrder (int orderId);
	 public List<Product> searching(String values) ;
	 
}
