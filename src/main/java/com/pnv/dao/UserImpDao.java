package com.pnv.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.pnv.model.ProductOrder;
import com.pnv.model.StaffInformation;
import com.pnv.utils.Utils;

@Service
@Transactional
public class UserImpDao implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void saveOrUpdate(StaffInformation staffInformation) {
		sessionFactory.getCurrentSession().save(staffInformation);
	}
	/*
	 * public boolean createNewStaff(StaffInformation staffInformation) {
	 * 
	 * Connection connection = DbHelper.returnDbHelper(); String createNewStaff
	 * = "INSERT INTO staff_information (name, address, phone," +
	 * " gender, identity_card, username, password, role_id, description_password) VALUES (?,?,?,?,?,?,MD5(?),?,?)"
	 * ;
	 * 
	 * try { PreparedStatement pre =
	 * connection.prepareStatement(createNewStaff); pre.setString(1,
	 * staffInformation.getName()); pre.setString(2,
	 * staffInformation.getAddress()); pre.setString(3,
	 * staffInformation.getPhone()); pre.setBoolean(4,
	 * staffInformation.isGender()); pre.setString(5,
	 * staffInformation.getIdentityCard()); pre.setString(6,
	 * staffInformation.getUserName()); pre.setString(7,
	 * staffInformation.getPassword()); //pre.setInt(8,
	 * staffInformation.getRole()); pre.setString(9,
	 * staffInformation.getPassword()); int notification = pre.executeUpdate();
	 * if (notification == 1) { return true; } connection.close();
	 * 
	 * } catch (SQLException e) { e.printStackTrace(); } return false; }
	 */

	public boolean deleteStaffById(int id) {
		String deleteStaff = "DELETE FROM StaffInformation WHERE staffId= :id";
		Query query = sessionFactory.getCurrentSession().createQuery(deleteStaff);
		query.setParameter("id", id);
		int check = query.executeUpdate();
		if (check != 0) {
			return true;
		}
		return false;
	}

	public int selectStaffId(String userName, String password) {

		String checkLogin = "FROM StaffInformation WHERE userName = :username and password = :pass";
		Query query = sessionFactory.getCurrentSession().createQuery(checkLogin);
		query.setParameter("username", userName);
		query.setParameter("pass", password);
		StaffInformation staff = (StaffInformation) query.uniqueResult();
		if (staff == null) {
			return 0;
		}
		return staff.getRole().getRoleId();
	}

	public boolean updatePassword(String userName, String password, String newPassword) {

		String pwmd5 = Utils.getMD5(password);
		String newpwmd5 = Utils.getMD5(newPassword);

		String changePassword = "UPDATE StaffInformation set password = :pass, descriptionPassword = :desPass "
				+ "WHERE userName = :user AND password = :pass1";
		Query query = sessionFactory.getCurrentSession().createQuery(changePassword);
		query.setParameter("pass", newpwmd5);
		query.setParameter("desPass", newPassword);
		query.setParameter("user", userName);
		query.setParameter("pass1", pwmd5);

		int result = query.executeUpdate();
		if (result != 0) {
			return true;
		}
		return false;
	}

	public List<StaffInformation> queryStaff() {
		String selectStaff = "from StaffInformation";
		Query query = sessionFactory.getCurrentSession().createQuery(selectStaff);
		List<StaffInformation> staff = query.list();
		return staff;
	}

	public StaffInformation showStaff(int staffId) {
		String select = "from StaffInformation Where staffId = :staffId";
		Query query = sessionFactory.getCurrentSession().createQuery(select);
		query.setParameter("staffId", staffId);
		StaffInformation staff = (StaffInformation) query.uniqueResult();
		return staff;
	}

	public boolean updateInformationOfStaff(int id, String address, String phone, String userName, String password) {

		String pwmd5 = Utils.getMD5(password);

		String updateInformation = "update StaffInformation set address= :address, phone= :phone,"
				+ "userName = :user, password = :pass, descriptionPassword = :dess where staffId = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(updateInformation);
		query.setParameter("address", address);
		query.setParameter("phone", phone);
		query.setParameter("user", userName);
		query.setParameter("pass", pwmd5);
		query.setParameter("dess", password);
		query.setParameter("id", id);
		int check = query.executeUpdate();
		if (check != 0) {
			return true;
		}
		return false;

	}

	public StaffInformation selectStaff(String userName, String password) {
		String checkLogin = "FROM StaffInformation WHERE userName = :username and password = :pass";
		Query query = sessionFactory.getCurrentSession().createQuery(checkLogin);
		query.setParameter("username", userName);
		query.setParameter("pass", password);
		StaffInformation staff = (StaffInformation) query.uniqueResult();
		if (staff == null) {
			return null;
		}
		return staff;
	}

	public StaffInformation updateInfoOfStaff(int id, String name, String address, String userName, String password) {
		String pwmd5 = Utils.getMD5(password);

		String updateInformation = "update StaffInformation set address= :address, name= :name,"
				+ "userName = :user, password = :pass, descriptionPassword = :dess where staffId = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(updateInformation);
		query.setParameter("address", address);
		query.setParameter("name", name);
		query.setParameter("user", userName);
		query.setParameter("pass", pwmd5);
		query.setParameter("dess", password);
		query.setParameter("id", id);
		if (query.executeUpdate() != 0) {
			StaffInformation staff = new StaffInformation(id, name, address, userName, password);

			return staff;

		}
		return null;
	}
}
