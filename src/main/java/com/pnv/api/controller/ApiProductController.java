package com.pnv.api.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.pnv.business.ProductBusiness;
import com.pnv.model.Order;
import com.pnv.model.Product;
import com.pnv.model.ResponseStatus;
import com.pnv.model.Size;

@Service
@Transactional
@Controller
@RequestMapping(value = "api/product")
public class ApiProductController {
	@Autowired
	private ProductBusiness show;
	
	@Autowired
	private ResponseStatus responseStatus;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<Product>viewDepartmentPage(ModelMap map) {
		List<Product> product_list = show.findAll();
		//map.put("product_list", product_list);
		return product_list;
	}
	//----------------------------------------------
	  @RequestMapping(value = "/addnew", method = RequestMethod.POST)
	    @ResponseBody
	    public ResponseStatus addOrEditDepartment( @RequestParam("productName") String productName,
	    		@RequestParam("quantity") Integer quantity,
	    		@RequestParam("dateUpdate") String dateUpdate,
	    		//@RequestParam("size") Size size,
	    		@RequestParam("size") Integer sizeId,
	    		@RequestParam("price") Float price,
	    		@RequestParam("description") String description,
	    		@RequestParam("status") Integer status,
	    		@RequestParam("imageFile") String imageFile,
	    		@RequestParam("imageName") String imageName,
	    		HttpServletRequest request) {
	        
		  Size size = new Size();
		  size.setSizeId(sizeId);
	      Product product= new Product(productName,quantity,dateUpdate,size,price,description,status);
	    	
	    	  
	    	 // logger.debug("imageFile   base64 character" +imageFile);
	    	  System.out.println("imageFile   base64 character" +imageFile);
	    	  
		      try { 
		    	  
		    	  
		    	  
		    	  if (!imageFile.isEmpty()) {
		              HttpSession session = request.getSession();
		              ServletContext sc = session.getServletContext();
		              String imagePath = sc.getRealPath("/") + "resources/images/";

		              File fileSaveDir = new File(imagePath);

		                  // if the directory does not exist, create it
		              if (!fileSaveDir.exists()) {
		                  System.out.println("creating directory: " + imagePath);
		                  boolean isCreated = false;

		                  try {
		                	  fileSaveDir.mkdir();
		                      isCreated = true;
		                  } catch (SecurityException se) {
		                      //handle it
		                  }
		                  if (isCreated) {
		                      System.out.println("DIR created");
		                  }
		              }

		              System.out.println("====imagePath=========");
		              System.out.println(imagePath);
		              System.out.println("=============");
		
		              try 
		              {               
		                  byte[] scanBytes = Base64.getDecoder().decode(imageFile);


		                                  ///////////////////////////////////////////////////////////////
		                  // Creating the directory to store file/data/image ////////////

//		                  String rootPath = System.getProperty("catalina.home");
	//
//		                  File fileSaveDir = new File(rootPath + File.separator + SAVE_DIR);
	//
//		                  // Creates the save directory if it does not exists
//		                  if (!fileSaveDir.exists()) 
//		                  {
//		                      fileSaveDir.mkdirs();
//		                  }

		                  File uploadFile = new File( fileSaveDir.getAbsolutePath() + File.separator + imageName);
		                  BufferedOutputStream scanStream = new BufferedOutputStream( new FileOutputStream( uploadFile ) );       
		                  scanStream.write(scanBytes);
		                  scanStream.close();

		                  //"RegisterSuccessful";
		                 // product.setImage(imageName);
		              }
		              catch (Exception e) 
		              {
		            	  System.out.println("You failed to upload scanImageFile => " + e.getMessage());
		                
		              }
		              
		           }
		    	  
		    	  
		    	 show.saveOrUpdate(product);
		         responseStatus.setDataObject(product);
		       } catch (Exception ex) {
		    	   responseStatus.setStatus(0);
		    	   responseStatus.setMessage(ex.getMessage());
		    	   System.out.println(ex.getMessage()) ;
		    	   
		    	   return responseStatus;
		       } 
		       
		      responseStatus.setStatus(1);
		      return responseStatus;
	    }  
    
	//---------------------------
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public ResponseStatus editTitle( @RequestBody Product product) {
       
    	//logger.debug("I am in the controller and got title name: " + title.toString());
	      try { 
	         show.saveOrUpdate(product);
	         responseStatus.setDataObject(product);
	       } catch (Exception ex) {
	    	   responseStatus.setStatus(0);
	    	   responseStatus.setMessage(ex.getMessage());
	    	   System.out.println(ex.getMessage()) ;
	    	   
	    	   return responseStatus;
	       } 
       
      responseStatus.setStatus(1);
      return responseStatus;
    }
	//----------------------------
	  @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	    public @ResponseBody ResponseStatus doDeleteTitle(@PathVariable("id") Integer id) {
	    
	    	try {
	    		show.delete(show.findByProductId(id));
	        	
	        } catch (Exception ex) {
	        	responseStatus.setStatus(0);
	        	responseStatus.setMessage(ex.getMessage());
	     	   System.out.println(ex.getMessage()) ;
	     	   
	     	   return responseStatus;
	        } 
	    	responseStatus.setStatus(1);
	        return responseStatus;

	    }
	
	//----------------------------

	@RequestMapping(value = "/showInformation/{id}", method = RequestMethod.GET)
	public @ResponseBody Product viewInfomationProductById(@PathVariable("id") Integer id, ModelMap map) {
		Product productById = show.findByProductId(id);
		return productById;
	}

	@RequestMapping(value = "/showProductMenu/{name}", method = RequestMethod.GET)
	public @ResponseBody List<Product>viewInfomationProductByName(@PathVariable("name") String name, ModelMap map) {	
		List<Product> productByName = show.findAllMenu(name);
		return productByName;
	}

	@RequestMapping(value = "/seaching", method = RequestMethod.POST)
	public @ResponseBody List<Product> doSearching(@RequestParam(value = "searchproduct", required = true) String text, ModelMap map) {
		  
		List<Product> product_list = show.searchAll(text);	
		   return product_list;
	}

}
