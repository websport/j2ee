package com.pnv.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pnv.business.ProductBusiness;
import com.pnv.business.UserBusiness;
import com.pnv.model.Product;
import com.pnv.model.ResponseStatus;
import com.pnv.model.Role;
import com.pnv.model.StaffInformation;
import com.pnv.utils.Utils;
import com.pnv.validation.FormValidation;

@Service
@Transactional
@Controller
@RequestMapping(value = "api/user")
public class ApiUserController {

	@Autowired
	private UserBusiness user;

	@Autowired
	private ResponseStatus responseStatus;

	@Autowired
	private ProductBusiness show;

	// @RequestMapping(value = "/", method = RequestMethod.GET)
	// public String viewWelcomePage(ModelMap map, HttpServletRequest request) {
	// HttpSession session = request.getSession(false);
	//
	// List<Product> listCl = show.showProductClub();
	// List<Product> listNa = show.showProductNational();
	// List<Product> listSB = show.showProductSB();
	//
	// try {
	// int staffId = (Integer) session.getAttribute("staffId");
	// if (staffId == 2) {
	// map.put("employeeList", listCl);
	// map.put("product", listNa);
	// map.put("products", listSB);
	// return "bossLogin";
	// } else {
	// map.put("employeeList", listCl);
	// map.put("product", listNa);
	// map.put("products", listSB);
	// return "staffLogin";
	// }
	// } catch (NullPointerException e) {
	// map.put("employeeList", listCl);
	// map.put("product", listNa);
	// map.put("products", listSB);
	// return "homePage";
	// }
	// }
	//
	// @RequestMapping(value = "/logout", method = RequestMethod.GET)
	// public String logOut(@ModelAttribute("staffInformation") StaffInformation
	// staff, ModelMap map,
	// HttpServletRequest request) {
	// HttpSession session = request.getSession(false);
	// session.invalidate();
	//
	// List<Product> listCl = show.showProductClub();
	// List<Product> listNa = show.showProductNational();
	// List<Product> listSB = show.showProductSB();
	// map.put("employeeList", listCl);
	// map.put("product", listNa);
	// map.put("products", listSB);
	//
	// return "homePage";
	//
	// }
	//
	// @RequestMapping(value = "/loginGet", method = RequestMethod.GET)
	// public String viewLoginPage(ModelMap map) {
	// return "login";
	// }

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus logIn(@RequestParam("email") String email, @RequestParam("pass") String password,
			HttpServletRequest request) {

		// HttpSession session = request.getSession();

		StaffInformation users;
		users = user.checkLogin1(email, password);
		if (users.getRole().getRoleId() == 2) {
			responseStatus.setMessage("Hello Boss");
		} else if (users.getRole().getRoleId() == 3) {
			responseStatus.setMessage("Hello employee");
		} else {
			responseStatus.setMessage("Sorry, please check!");
		}
		responseStatus.setDataObject(users);
		return responseStatus;
	}

	@RequestMapping(value = "/showStaff", method = RequestMethod.GET)
	public @ResponseBody List<StaffInformation> viewAllOfStaff() {
		List<StaffInformation> showStaff = new ArrayList<StaffInformation>();
		showStaff = user.showStaff();
		return showStaff;
	}

	@RequestMapping(value = "/editInforfStaff", method = RequestMethod.POST)
	public @ResponseBody StaffInformation editStaffs(@RequestParam("staffId") String id,
			@RequestParam("name") String name, @RequestParam("address") String Address,
			@RequestParam("username") String username, @RequestParam("password") String password,
			HttpServletRequest request) {
		StaffInformation staff = new StaffInformation(Integer.parseInt(id), name, Address, username, password);
		StaffInformation staff1 = user.changeInforOfStaff(Integer.parseInt(id), staff.getName(), staff.getAddress(),
				staff.getUserName(), staff.getPassword());
		return staff1;
	}

	@RequestMapping(value = "/editForm/{staffId}", method = RequestMethod.GET)
	public @ResponseBody StaffInformation findStaff(@PathVariable("staffId") Integer staffId, ModelMap map) {
		StaffInformation staff = user.findUser(staffId);
		return staff;
	}

	//
	// @RequestMapping(value = "/editInformationOfStaff", method =
	// RequestMethod.POST)
	// public String editStaff(@ModelAttribute("staffInfor") StaffInformation
	// staff, ModelMap map,
	// @RequestParam("id") int id) {
	// String errorAddressInput =
	// FormValidation.addressValidation(staff.getAddress());
	// String errorPhoneInput =
	// FormValidation.phoneNumberInputValidation(staff.getPhone());
	// String errorUserNameInput =
	// FormValidation.emailInputValidation(staff.getUserName());
	// String errorPasswordInput =
	// FormValidation.passwordValidation(staff.getPassword());
	//
	// if (errorPhoneInput.equals("") && errorAddressInput.equals("") &&
	// errorUserNameInput.equals("")
	// && errorPasswordInput.equals("")) {
	// boolean result = user.changeInformationOfStaff(id, staff.getAddress(),
	// staff.getPhone(),
	// staff.getUserName(), staff.getPassword());
	// if (result) {
	// List<StaffInformation> showStaff = new ArrayList<StaffInformation>();
	// showStaff = user.showStaff();
	// map.addAttribute("user", showStaff);
	// map.addAttribute("MS", "Chỉnh sửa thành công!!!");
	// return "showStaff";
	// } else {
	// StaffInformation staff1 = user.findUser(id);
	// map.addAttribute("edit", staff1);
	// map.addAttribute("MS", "Chỉnh sửa thất bại!!!");
	// return "editAccount";
	// }
	// }
	// StaffInformation staff1 = user.findUser(id);
	// map.addAttribute("edit", staff);
	// map.addAttribute("MS", errorPhoneInput + errorAddressInput +
	// errorUserNameInput + errorPasswordInput);
	// return "editAccount";
	// }
	//
	// @RequestMapping(value = "/delete/{staffId}", method = RequestMethod.GET)
	// public String deleteStaff(@PathVariable("staffId") Integer staffId,
	// ModelMap map) {
	// boolean deleteUser = user.deleteStaffById(staffId);
	// if (deleteUser) {
	// List<StaffInformation> showStaff = new ArrayList<StaffInformation>();
	// showStaff = user.showStaff();
	// map.addAttribute("user", showStaff);
	// map.addAttribute("MS", "Xóa thành công!!!");
	// return "showStaff";
	// }
	// List<StaffInformation> showStaff = new ArrayList<StaffInformation>();
	// showStaff = user.showStaff();
	// map.addAttribute("user", showStaff);
	// map.addAttribute("MS", "Xóa thất bại!!!");
	// return "showStaff";
	// }
	//
	// @RequestMapping(value = "/registerGet", method = RequestMethod.GET)
	// public String viewRegisterForm(ModelMap map) {
	// return "register";
	// }
	//
	// @RequestMapping(value = "/registerPost", method = RequestMethod.POST)
	// public String registerAccount(@RequestParam("name") String name,
	// @RequestParam("phone") String phone,
	// @RequestParam("address") String address, @RequestParam("gender") String
	// gender,
	// @RequestParam("identityCard") String identityCard,
	// @RequestParam("userName") String userName,
	// @RequestParam("password") String password, @RequestParam("role") String
	// role, ModelMap map) {
	//
	// String errorFullNameInput = FormValidation.fullNameValidation(name);
	// String errorAddressInput = FormValidation.addressValidation(address);
	// String errorPhoneInput =
	// FormValidation.phoneNumberInputValidation(phone);
	// String errorIdentityCardInput =
	// FormValidation.identityCardInputValidation(identityCard);
	// String errorUserNameInput =
	// FormValidation.emailInputValidation(userName);
	// String errorPasswordInput = FormValidation.passwordValidation(password);
	//
	// if (errorFullNameInput.equals("") && errorAddressInput.equals("") &&
	// errorPhoneInput.equals("")
	// && errorIdentityCardInput.equals("") && errorUserNameInput.equals("")
	// && errorPasswordInput.equals("")) {
	// int iRole = Integer.parseInt(role);
	// Role ro = new Role();
	// ro.setRoleId(iRole);
	// boolean result;
	// String pwmd5 = Utils.getMD5(password);
	// if (gender.equals("1")) {
	// StaffInformation staff = new StaffInformation(name, address, phone, true,
	// identityCard, userName, pwmd5,
	// password, ro);
	// result = user.saveOrUpdate(staff);
	// } else {
	// StaffInformation staff = new StaffInformation(name, address, phone, true,
	// identityCard, userName, pwmd5,
	// password, ro);
	// result = user.saveOrUpdate(staff);
	// }
	// if (result == true) {
	// map.addAttribute("MS", "Register successfully !!!");
	// return "bossLogin";
	// } else {
	// map.addAttribute("MS", "Register unsuccessfully. Please check your
	// information !!!");
	// return "register";
	// }
	// } else {
	// map.addAttribute("MS", errorFullNameInput + errorAddressInput +
	// errorPhoneInput + errorIdentityCardInput
	// + errorUserNameInput + errorPasswordInput);
	// return "register";
	// }
	//
	// }
	//
	//
	@RequestMapping(value = "/changePasswordPost", method = RequestMethod.POST)
	public @ResponseBody ResponseStatus checkChangePassword(@RequestParam("userName") String userName, @RequestParam("pass") String pass,
			@RequestParam("newPassword") String newPassword, @RequestParam("confirmPassword") String confirmPassword,
			ModelMap map, HttpServletRequest request) {

		if (newPassword.equals(confirmPassword)) {
			boolean result = user.changePassword(userName, pass, newPassword);
			if (result) {
				responseStatus.setMessage("Change password successfully");
			}else{
				responseStatus.setMessage("Change password unsuccessfully, please check");
			}
		}
		responseStatus.setMessage("Please confirm password again");
		return responseStatus;

	}
}
