  <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <footer class="footer" >
    <div class="footer-middle" >
      <div class="container">
          <div class="col-xs-12 col-sm-3">
          <div class="blog_inner">
            <div class="blog-img blog-l"> <img src="<%=request.getContextPath() %>/resources/images/product_footer.png" alt="Blog image">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              
            <div class="footer-logo"><a href="index.html" title="Logo"><img src="<%=request.getContextPath() %>/resources/images/logo.png" alt="logo"></a></div>
            <address>
            <i class="add-icon">&nbsp;</i>80B LÊ DUẨN <br>
            &nbsp;
            </address>
            <div class="phone-footer"><i class="phone-icon">&nbsp;</i> +511 3 113 113</div>
            <div class="email-footer"><i class="email-icon">&nbsp;</i> <a href="">support@sdshop.com</a> </div>
          </div>
          
          <div class="col-lg-3 col-md-8 col-sm-9 col-xs-12">
            <div class="social"  >
            </br></br></br>
              <h4>Follow Us</h4>
              <ul>
                <li class="fb"><a href="#"></a></li>
                <li class="tw"><a href="#"></a></li>
                <li class="googleplus"><a href="#"></a></li>
                <li class="youtube"><a href="#"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom" style="margin:0px;">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-xs-12 coppyright"> &copy; 2016 SD SHOP. All Rights Reserved.</div>
          <div class="col-sm-8 col-xs-12 company-links">
            <ul class="links">
              <li><a href="#" title="Magento Themes">Nguyễn Thị Mai</a></li>
              <li><a href="#" title="Premium Themes">Nguyễn Hữu Tường Vi</a></li>
              <li><a href="#" title="Responsive Themes">Nguyễn Ngọc Đa</a></li>
              <li><a href="#" title="Responsive Themes">Lý Thị Trung Nhẫn</a></li>
              <li class="last"><a href="#" title="Magento Extensions">Đinh Thanh Hà</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>