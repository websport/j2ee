<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<section class="slider-intro">
    <div class="container">
      <div class="the-slideshow slideshow-wrapper">
        <ul style="overflow: hidden;" class="slideshow">
          <li class="slide">
            <p><a href="#"> <img src="<%=request.getContextPath() %>/resources/images/slide_ronaldo.jpg" alt=""></a></p>
            <div class="caption light1 top-right">
              <div class="caption-inner">
                <p class="heading">Football clothes 2016</p>
              </div>
            </div>
          </li>
          <li class="slide">
            <p><a href="#"> <img src="<%=request.getContextPath() %>/resources/images/slide_messi.jpg" alt=""></a></p>
            <div class="caption light top-right">
              <div class="caption-inner">
                <p class="normal-text">Collection 2014</p>
                <h2 class="heading permanent">You like me</h2>
              </div>
            </div>
          </li>
          <li class="slide">
            <p><a title="#" href="#"> <img src="<%=request.getContextPath() %>/resources/images/slide_giroud.jpg" alt=""> </a></p>
            <div class="caption light2 top-right">
              <div class="caption-inner">
                <p class="heading">Come With SD Shop</p>
              </div>
            </div>
          </li>
        </ul>    
      </div>
    </div>
  </section>