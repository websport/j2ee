 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <nav>
    <div class="container">
      <div class="nav-inner"> 
        <!-- mobile-menu -->
        <div class="hidden-desktop" id="mobile-menu">
          <ul class="navmenu">
            <li>
              <div class="menutop">
                <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
              </div>
		</ul>
        </div>
        <ul id="nav" class="hidden-xs">
          <li id="nav-home" class="level0 parent drop-menu active"><a href="<%=request.getContextPath()%>/"><span>Home</span> </a> </li>
          <li class="level0 parent drop-menu"><a href="#"><span>Clothes</span> </a>
            <ul style="display: none;" class="level1">
           
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Arsenal"><span>Arsenal</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Manchester United"><span>Manchester United</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Barcelona"><span>Barcelona</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Bayern Munich"><span>Bayern Munich</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Manchester City"><span>Manchester City</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Real Madrid"><span>Real Madrid</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Chelsea"><span>Chelsea</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Liverpool"><span>Liverpool</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Juventus"><span>Juventus</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Paris Saint Germain"><span>Paris Saint-Germain</span></a></li>
          
            </ul>
          </li>
           <li class="level0 parent drop-menu"><a href="#"><span>Shoes</span> </a>
            <ul style="display: none;" class="level1">
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Giay_Adidas"><span>Adidas</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Giay_Nike"><span>Nike</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Giay_Warriol"><span>Warriol</span></a></li>
            </ul>
          </li>
          <li class="level0 parent drop-menu"><a href="#"><span>Ball</span> </a>
            <ul style="display: none;" class="level1">
                  
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Bong_Nike"><span>Nike</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Bong_New_Balance"><span>Balance</span></a></li>
                  <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Bong_Adidas"><span>Adidas</span></a></li>
                   <li class="level1"><a href="<%=request.getContextPath()%>/product/showProductMenu/Bong_Dynamics"><span>Dynamics</span></a></li>
            </ul>
          </li>
        </ul>
        <div class="search-box">
          <form id="search_mini_form" name="Categories" action="<%=request.getContextPath() %>/product/seaching" method="post">
            <input type="text" placeholder="Search product here.." maxlength="70" name="searchproduct" id="search" >
            <button  class="btn btn-default  search-btn-bg" value="search" type="submit"> <span class="glyphicon glyphicon-search" ></span>&nbsp;</button>
          </form>          
        </div>
      </div>
    </div>
  </nav>