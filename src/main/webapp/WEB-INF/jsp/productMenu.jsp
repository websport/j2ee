<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Shop Sport</title>
<link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/slider.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.theme.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/style.css" type="text/css">
</head>
<body class="cms-index-index">
<div class="page"> 
  <!-- Header -->
 <%@include file = "header1.jsp"%>
  <!-- end header --> 
  <!-- Navbar -->
<%@include file = "menu.jsp"%>   
    <section class="main-container col1-layout home-content-container">
    <div class="container">
      <div class="row">
        <div class="std">
          <div class="best-pro col-lg-12">
            <div class="slider-items-products">
              <div class="new_title center">
                <h2>Clothes follow category</h2>
              </div>           
              <div id="best-seller-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4"> 
                  <c:forEach var="Arsenal" items="${ListClub}">   
                  <!-- Item -->
                  <div class="item">
                    <div class="col-item">
                      <div class="item-inner">
                        <div class="item-img">

                          <div class="item-img-info"> <a href="<%=request.getContextPath()%>/product/showInformation/${Arsenal.productId}"  title="Sample Product" class="product-image"> <img src="<%=request.getContextPath() %>/resources/images/${Arsenal.image}" alt="Sample Product"> </a>
							<div class="item-box-hover">
                              <div class="box-inner">
                                <div class="product-detail-bnt"><a href="<%=request.getContextPath()%>/product/showInformation/${Arsenal.productId}" class="button detail-bnt"> <span> Quick View</span></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a href="product_detail.html">${Arsenal.productName} </a> </div>
                            <div class="item-content">
                              <div class="rating">
                                <div class="ratings">
                                  <div class="rating-box">
                                    <div class="rating" style="width:80%"></div>
                                  </div>
                                  <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Review</a> </p>
                                </div>
                              </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price" id="product-price-1"> <span class="price">${Arsenal.price}00 VND</span> </span> </div>
                              </div>
                            </div>
                          </div>
                          <div class="actions"><span class="add-to-links"> <a href="wishlist.html" class="link-wishlist" title="Add to Wishlist"><span>Add to Wishlist</span></a>
                            <button  title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                            <a href="compare.html" class="link-compare" title="Add to Compare"><span>Add to Compare</span></a></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End Item -->   
                   </c:forEach> 
                       </div>
              </div>
            </div>
          </div>
<!--  <a href="<%=request.getContextPath() %>/productController?formAction=showProductArsenal&page=1">1</a>  
<a href="<%=request.getContextPath() %>/productController?formAction=showProductArsenal&page=2">2</a>  
<a href="<%=request.getContextPath() %>/productController?formAction=showProductArsenal&page=3">3</a>  --> 
 <!-- Footer -->
<%@include file = "footer.jsp"%>
</div>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/owl.carousel.min.js"></script>
	<script type="text/javascript">

    //<![CDATA[
	jQuery(function() {
		jQuery(".slideshow").cycle({
			fx: 'scrollHorz', easing: 'easeInOutCubic', timeout: 10000, speedOut: 800, speedIn: 800, sync: 1, pause: 1, fit: 0, 			pager: '#home-slides-pager',
			prev: '#home-slides-prev',
			next: '#home-slides-next'
		});
	});
    //]]>
    </script>
</body> 
</body>
</html>