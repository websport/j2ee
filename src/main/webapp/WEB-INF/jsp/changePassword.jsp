<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Accord, premium HTML5 &amp; CSS3 template</title>
<link type ="text/css" rel="stylesheet" href ="<%=request.getContextPath() %>/resources/css/login.css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/slider.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/owl.theme.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/style.css" type="text/css">
</head>
<body class="cms-index-index">
	<div class="page">
		<!-- Header -->
		<header class="header-container">
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-xs-6">
							<div class="welcome-msg hidden-xs">
								<h2>Welcome to SD Shop</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header container">
				<div class="row">
					<div class="col-lg-3 col-sm-4 col-md-3">>
						<div class="logo">
							<a title="Magento Commerce" href=""><img
								alt="Magento Commerce"
								src="<%=request.getContextPath()%>/resources/images/image_SDshop.png"></a>
						</div>
					</div>
					<div class="col-lg-9 col-xs-12">
						<div class="top-cart-contain">
							<div class="mini-cart">
								<div data-toggle="dropdown" data-hover="dropdown"
									class="basket dropdown-toggle">
									<a href="#"><span>Shopping Cart</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<%@include file="menu.jsp"%>
		<div class="container">
			<div class="card card-container">
				<img id="profile-img" class="profile-img-card"
					src="<%=request.getContextPath()%>/resources/images/changepass.png" />
				<p id="profile-name" class="profile-name-card"></p>
				<form class="form-signin" action="changePasswordPost" method="POST">
					<span id="reauth-email" class="reauth-email"></span> <input
						type="password" id="inputPassword" name="pass"
						class="form-control" placeholder="Password" required> <input
						type="password" id="inputPassword" name="newPassword"
						class="form-control" placeholder="New password" required>
					<input type="password" id="inputPassword" name="confirmPassword"
						class="form-control" placeholder="Confirm password" required>
					<button class="btn btn-lg btn-primary btn-block btn-signin"
						type="submit" name="action" value="changePassword">Change
						password</button>
				</form>
				<form class="form-signin"
					action="<%=request.getContextPath()%>/" method="GET">
					<button class="btn btn-lg btn-primary btn-block btn-signin"
						type="submit" onclick="javascript:history.go(-1)">CANCEL</button>
				</form>
				<%@include file="message.jsp"%>
			</div>
		</div>
		<%@include file="footer.jsp"%>
	</div>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/parallax.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/common.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/slider.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(function() {
			jQuery(".slideshow").cycle({
				fx : 'scrollHorz',
				easing : 'easeInOutCubic',
				timeout : 10000,
				speedOut : 800,
				speedIn : 800,
				sync : 1,
				pause : 1,
				fit : 0,
				pager : '#home-slides-pager',
				prev : '#home-slides-prev',
				next : '#home-slides-next'
			});
		});
		//]]>
	</script>
</body>