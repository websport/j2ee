<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<header class="header-container">
    <div style="background:#ffffff; " class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-xs-6">            

              <div class="welcome-msg hidden-xs"><h2>Welcome to SD Shop</h2></div>
          </div>
          <div class="col-lg-7 col-md-7 col-xs-6"> 
            
            <!-- Header Top Links -->
            <div class="toplinks">
            	</br>
              <div class="login">
              <a href = "<%=request.getContextPath() %>/loginGet" >
                  <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
                 </a>
              </div>
    		
            </div>
            <!-- End Header Top Links --> 
          </div>
        </div>
      </div>
    </div>
   
    <div class="header container">
      <div class="row">
        <div class="col-lg-3 col-sm-4 col-md-3"> 
          <!-- Header Logo -->
          <div class="logo"> <a title="Magento Commerce" href=""><img alt="Magento Commerce" src="<%=request.getContextPath() %>/resources/images/image_SDshop.png"></a> </div>
          <!-- End Header Logo --> 
        </div>
        <div class="col-lg-9 col-xs-12">
          <div class="top-cart-contain">
            <div class="mini-cart">
              <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"><span>Shopping Cart</span></a></div>
  
            </div>
           
          </div>
        </div>
      </div>
    </div>
   </header>