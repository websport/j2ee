<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Edit Or Insert Product</title>
<link rel="icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<link rel="shortcut icon"
	href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico"
	type="image/x-icon" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/slider.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.carousel.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/owl.theme.css" type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/font-awesome.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/style.css" type="text/css">
 <style>
table {
    border-collapse: collapse;
    width: 450px;
    height: 320px;
    font-size: 15px;
}

th, td {
    text-align: left;
    padding: 8px;
}
</style> 
<%@include file="include.jsp"%>
</head>
<body>
<div class="page">
<%@include file="header1.jsp"%>
<%@include file="menu.jsp"%>
  <center><h3><b>Insert Product</b></h3>
         <h4><font color="red">${requestScope["message"]}</font></h4>
 <div class="container">
			<div class="card card-container"> 
	<center>
		 <form:form action="add" modelAttribute="productForm" method="POST"  enctype="multipart/form-data">
            <form:hidden path="productId" />
			<table border="0" width="90%">
				<tr>
					<td>Product Name</td>
					<td><form:input type="text" path="productName" size="30"/></td>	
					<td><span style="color: red;"><form:errors path="productName" cssClass="error"/></span></td>
				</tr>	
				<tr>
					<td>Quantity</td>
					<td><form:input type="text" path="quantity" size="30"/></td>	
					<td><span style="color: red"><form:errors path="quantity" cssClass="error"/></span></td>
				</tr>
				<tr>
					<td>Date Update</td>
					<td><form:input type="date" path="dateUpdate" size="30"/></td>
					<td><span style="color: red"><form:errors path="dateUpdate" cssClass="error"/></span></td>
				</tr>
				
				<tr>
					<td>Size</td>
					<td><form:select path="size.sizeId"><b>
						<form:option value="1">S</form:option>
						<form:option value="2">M</form:option>
						<form:option value="3">L</form:option>
						<form:option value="4">XL</form:option>
						<form:option value="5">XXL</form:option>
						<form:option value="6">XXXL</form:option>
						<form:option value="7">1</form:option>
						<form:option value="8">2</form:option>
						<form:option value="9">3</form:option>
						<form:option value="10">4</form:option>
						<form:option value="11">5</form:option>
						<form:option value="12">32</form:option>
						<form:option value="13">33</form:option>
						<form:option value="14">34</form:option>
						<form:option value="15">35</form:option>
						<form:option value="16">36</form:option>
						<form:option value="17">37</form:option>
						<form:option value="18">38</form:option>
						<form:option value="19">39</form:option>
						<form:option value="20">40</form:option>
						<form:option value="21">41</form:option>
						<form:option value="22">42</form:option>
						<form:option value="23">43</form:option>
						<form:option value="24">44</form:option>
						<form:option value="25">45</form:option>
					</b></form:select></td>
				</tr>
				<tr>
					<td>Price</td>
					<td> <form:input type="text" path="price" size="30"/></td>
					<td><span style="color: red"><form:errors path="price" cssClass="error"/></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><form:input type="text" path="description" size="30"/></td>
					<td><span style="color: red"><form:errors path="description" cssClass="error"/></span></td>
				</tr>
				<tr>
					<td>Image</td>
					<td><input type="file" name="file"></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><form:select path="status">
					    <form:option value="0">Club Clothes</form:option>
						<form:option value="1">National Clothes</form:option>
						<form:option value="2">Shoes and Ball</form:option>
						</form:select></td>
				</tr>
			</table>
			<br> <!--  <input type="submit" style="background-color: #3a96f2" name="action" value="Insert" />-->
			     <button type="submit" style="background-color: #7c7cd3" name="action" value="submit" class="button btn-cart"><b>INSERT PRODUCT</b></button>
			     <button type="submit" style="background-color: #7c7cd3"value="Back" onclick="javascript:history.go(-1)" class="button btn-cart"><b>BACK</b></button>
	   </form:form>
	</center>
</div>
</div>
<%@include file="footer.jsp"%>
</div>
</body>
</html>
